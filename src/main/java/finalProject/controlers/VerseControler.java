package finalProject.controlers;

import com.google.common.collect.Lists;
import finalProject.entities.Favourite;
import finalProject.entities.Song;
import finalProject.entities.Verse;
import finalProject.repositories.SongRepository;
import finalProject.repositories.SongRepositoryInstance;
import finalProject.repositories.VerseRepository;
import finalProject.repositories.VerseRepositoryInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class VerseControler {

    @Autowired
    SongRepository songRepository;

    @Autowired
    SongRepositoryInstance songRepositoryInstance;

    @Autowired
    VerseRepository verseRepository;

    @Autowired
    VerseRepositoryInstance verseRepositoryInstance;


    @GetMapping("/song/{id}")
    public String allVersesOfSong (@PathVariable("id") Integer id, Model model) {

        List<Song> songs = Lists.newArrayList(songRepository.findAll());
        model.addAttribute("songs", songs);

        List<Verse> mySong = verseRepositoryInstance.findAllVersesBySongId(id);
        model.addAttribute("mySong", mySong);

        ArrayList allFavouriteLists = Lists.newArrayList(songRepositoryInstance.findAllFavouriteLists());
        model.addAttribute("allLists", allFavouriteLists);
        return "song";
    }

    @GetMapping("/harcerskieSong/{id}")
    public String allVersesOfHarcerskie (@PathVariable("id") Integer id, Model model) {

        List<Song> songId1 = songRepositoryInstance.findAllSongsfromCategory(1);
        model.addAttribute("songId1", songId1);

        List<Verse> mySongId1 = verseRepositoryInstance.findAllVersesBySongId(id);
        model.addAttribute("mySongId1", mySongId1);
        return "harcerskieSong";
    }

    @GetMapping("/chrzescijanskieSong/{id}")
    public String allVersesOfChrzescijanskie (@PathVariable("id") Integer id, Model model) {

        List<Song> songId2 = songRepositoryInstance.findAllSongsfromCategory(2);
        model.addAttribute("songId2", songId2);

        List<Verse> mySongId2 = verseRepositoryInstance.findAllVersesBySongId(id);
        model.addAttribute("mySongId2", mySongId2);
        return "chrzescijanskieSong";
    }

    @GetMapping("/szantySong/{id}")
    public String allVersesOfSzanty (@PathVariable("id") Integer id, Model model) {

        List<Song> songId3 = songRepositoryInstance.findAllSongsfromCategory(3);
        model.addAttribute("songId3", songId3);

        List<Verse> mySongId3 = verseRepositoryInstance.findAllVersesBySongId(id);
        model.addAttribute("mySongId3", mySongId3);
        return "szantySong";
    }

    @GetMapping("/inneSong/{id}")
    public String allVersesOfInne (@PathVariable("id") Integer id, Model model) {

        List<Song> songId4 = songRepositoryInstance.findAllSongsfromCategory(4);
        model.addAttribute("songId4", songId4);

        List<Verse> mySongId4 = verseRepositoryInstance.findAllVersesBySongId(id);
        model.addAttribute("mySongId4", mySongId4);
        return "inneSong";
    }

    @GetMapping("/searchSong/{search}/{id}")
    public String searchSongs(@PathVariable("search") String search, @PathVariable("id") Integer id, Model model) {
        List<Song> searchSong = songRepositoryInstance.findAllSongsBySearch(search);
        model.addAttribute("searchSong", searchSong);
        List<Verse> mySearchSong = verseRepositoryInstance.findAllVersesBySongId(id);
        model.addAttribute("mySearchSong", mySearchSong);
        model.addAttribute("searchQuery", search);
        return "searchSong";
    }

}
