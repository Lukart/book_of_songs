package finalProject.repositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

@Repository
@Transactional
public class SongRepositoryInstance {

    @Autowired
    EntityManager em;

    public List findAllSongsfromCategory(Integer categoryId) {
        return em.createQuery("Select v from Song v where v.categorie = " + categoryId).getResultList();
    }

    public List findAllSongs() {
        return em.createQuery("Select title from Song").getResultList();
    }

    public List findAllSongsBySearch(String searchText) {
        return em.createQuery("Select v from Song v where v.title  like '%" + searchText + "%'").getResultList();
    }

    public List findAllFavouriteLists() {
        return em.createQuery("Select name from favourite").getResultList();
    }

    public Integer IdFromNamedFavouriteList(String nameOfList) {
        return (int)em.createQuery("Select id from favourite where name = \'" + nameOfList + "\'").getSingleResult();
    }

    public boolean isAddToFavouriteList(String listName, Integer songId){
        Object listId = em.createQuery("Select favourite_id from Song where Song.id = " + songId).getSingleResult();
        if (listId != null) {
            String nameOfList = String.valueOf(em.createQuery("SELECT name from favourite where id = " + (int)listId).getSingleResult());
            return nameOfList.equals(listName);
        }
        return false;
    }
}