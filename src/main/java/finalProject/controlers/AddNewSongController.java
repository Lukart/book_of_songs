package finalProject.controlers;

import finalProject.repositories.SongRepository;
import finalProject.repositories.SongRepositoryInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AddNewSongController {

    @Autowired
    SongRepository songRepository;

    @Autowired
    SongRepositoryInstance songRepositoryInstance;

    @Autowired
    EntityManager em;

    @GetMapping("/addNewSong/addFailed")
    public String addFailed() {
        return "newSong/addFailed";
    }

    @GetMapping("/addNewSong/addConfirmed")
    public String addConfirmed() {
        return "newSong/addConfirmed";
    }

    @GetMapping("/addNewSong")
    public String add() {
        return "newSong/add";
    }

    @PostMapping("/addNewSong")
    @Transactional
    public String addOneSong(@RequestParam("category_area") Integer categorie,
                             @RequestParam("title_area") String title,
                             @RequestParam("text_area") String allText,
                             @RequestParam("chords_area") String allChords) {
        String[] verses = allText.split("\r\n");
        String[] chords = allChords.split("\r\n");
        List<String> finalVerses = new ArrayList<>();
        List<String> finalChords = new ArrayList<>();
        addSpaceLineInHTML(verses, finalVerses);
        addSpaceLineInHTML(chords, finalChords);

        try {
            for (int i = 0; i < songRepositoryInstance.findAllSongs().size(); i++) {
                if (title.equals(songRepositoryInstance.findAllSongs().get(i))) {
                    return "redirect:/addNewSong/addFailed";
                }
            }
            em.joinTransaction();
            em.createNativeQuery("INSERT INTO song (title, categorie_id) VALUES(:a, :b)")
                    .setParameter("a", title)
                    .setParameter("b", categorie).
                    executeUpdate();
            int songID = (int) em.createNativeQuery("SELECT max(id) FROM song").getSingleResult();

            for (int i = 0; i < finalVerses.size(); i++) {
                em.createNativeQuery("INSERT INTO verse (text_of_verse, chords_of_verse, number_of_verse, song_id) VALUES (:a, :b, :c, :d)")
                        .setParameter("a", finalVerses.get(i))
                        .setParameter("b", finalChords.get(i))
                        .setParameter("c", (i + 1))
                        .setParameter("d", songID)
                        .executeUpdate();
            }
        } catch (Exception e) {
            return "redirect:/addNewSong/addFailed";
        }
        return "redirect:/addNewSong/addConfirmed";
    }

    private void addSpaceLineInHTML(String[] text, List<String> finalText) {
        for (int i = 0; i < text.length; i++) {
            if (text[i].isEmpty()) {
                finalText.add("<br/>");
            } else {
                finalText.add(text[i]);
            }
        }
    }
}
