package finalProject.controlers;

import com.google.common.collect.Lists;
import finalProject.repositories.SongRepositoryInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.persistence.EntityManager;
import java.util.ArrayList;

@Controller
public class MyFavouriteListController {

    @Autowired
    EntityManager em;

    @Autowired
    SongRepositoryInstance songRepositoryInstance;

    @GetMapping("/myFavouriteLists")
    public String myFavouriteLists(Model model) {
        ArrayList favouriteLists = Lists.newArrayList(songRepositoryInstance.findAllFavouriteLists());
        model.addAttribute("favLists", favouriteLists);
        return "myFavouriteLists/myFavouriteLists";
    }

    @PostMapping("/myFavouriteLists")
    @Transactional
    public String addNewFavouriteList(@RequestParam("list_title_area") String listTitle) {
        if (!songRepositoryInstance.findAllFavouriteLists().isEmpty()) {
            for (int i = 0; i < songRepositoryInstance.findAllFavouriteLists().size(); i++) {
                if (listTitle.equals(songRepositoryInstance.findAllFavouriteLists().get(i)))
                    return "myFavouriteLists/addListFailedIsExist";
            }
        }
        if (!listTitle.isEmpty()) {
            //em.createNativeQuery("CREATE table " + listTitle + "(id)").executeUpdate();


            em.createNativeQuery("INSERT  into favourite (name, user_id) VALUES (:a, :b)")
                    .setParameter("a", listTitle)
                    .setParameter("b", null)
                    .executeUpdate();
            return "myFavouriteLists/addListConfirmed";
        }
        return "myFavouriteLists/addListFailed";
    }

    @PostMapping("/myFavouriteLists/addSong")
    @Transactional
    public String addSongToFavouriteList(@RequestParam("name_favourite_list") String listName,
                                         @RequestParam("songId2") Integer songId) {
        int listId = songRepositoryInstance.IdFromNamedFavouriteList(listName);
        boolean isAdd = songRepositoryInstance.isAddToFavouriteList(listName, songId);
        if(!isAdd) {
            em.createNativeQuery("UPDATE Song set favourite_id = " + listId + " WHERE Song.id = " + songId).executeUpdate();
            return "myFavouriteLists/addSongToFavouriteListConfirmed";
        }else return "myFavouriteLists/addSongToFavouriteListFailed";
    }
}


