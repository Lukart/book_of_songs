package finalProject.controlers;

import com.google.common.collect.Lists;
import finalProject.entities.Song;
import finalProject.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.persistence.EntityManager;
import java.util.List;

@Controller
public class SongControler {

    @Autowired
    SongRepository songRepository;

    @Autowired
    SongRepositoryInstance songRepositoryInstance;

    @Autowired
    EntityManager em;

    @GetMapping("/all")
    public String allSongs(Model model) {

        List<Song> songs = Lists.newArrayList(songRepository.findAll());
        model.addAttribute("songs", songs);
        return "all";
    }

    @GetMapping("/harcerskie")
    public String harcerskieSongs(Model model) {

        List<Song> songId1 = songRepositoryInstance.findAllSongsfromCategory(1);
        model.addAttribute("songId1", songId1);
        return "harcerskie";
    }

    @GetMapping("/chrzescijanskie")
    public String chrzescijanskieSongs(Model model) {

        List<Song> songId2 = songRepositoryInstance.findAllSongsfromCategory(2);
        model.addAttribute("songId2", songId2);
        return "chrzescijanskie";
    }

    @GetMapping("/szanty")
    public String szantySongs(Model model) {

        List<Song> songId3 = songRepositoryInstance.findAllSongsfromCategory(3);
        model.addAttribute("songId3", songId3);
        return "szanty";
    }

    @GetMapping("/inne")
    public String inneSongs(Model model) {

        List<Song> songId4 = songRepositoryInstance.findAllSongsfromCategory(4);
        model.addAttribute("songId4", songId4);
        return "inne";
    }
}