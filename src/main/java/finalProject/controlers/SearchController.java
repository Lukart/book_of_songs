package finalProject.controlers;

import finalProject.entities.Song;
import finalProject.repositories.SongRepositoryInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SearchController {

    @Autowired
    SongRepositoryInstance songRepositoryInstance;

    @GetMapping("/search")
    public String searchSongs(Model model, @RequestParam("search") String search) {
        List<Song> searchSong = songRepositoryInstance.findAllSongsBySearch(search);
        model.addAttribute("searchSong", searchSong);
        model.addAttribute("searchQuery", search);
        return "search";
    }
}
