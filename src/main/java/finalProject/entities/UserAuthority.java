package finalProject.entities;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "authorities")
public class UserAuthority {

    @Id
    public String username;
    public String authority;

}
