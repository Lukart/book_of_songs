package finalProject.controlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import java.math.BigInteger;

@Controller
public class DeleteSongController {

    @Autowired
    EntityManager em;

    @Transactional
    @PostMapping("/deleteSong")
    public String deleteSongByID(@RequestParam("songId1") String songID,
                                 @RequestParam("button_delete") Boolean delete) {
        int isExist = ((BigInteger) em.createNativeQuery("SELECT count(1) FROM song where id=" + songID).getSingleResult()).intValue();
        if (delete && isExist == 1) {
            em.createNativeQuery("DELETE from song where id=" + songID).executeUpdate();
            em.createNativeQuery("DELETE v from verse v where v.song_id=" + songID).executeUpdate();
            return "deleteConfirmed";
        }
        return "deleteFailed";
    }
}
